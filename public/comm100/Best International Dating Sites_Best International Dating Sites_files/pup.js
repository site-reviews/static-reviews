document.body.onmouseleave = function(){
    let exit = document.getElementsByClassName('exit-popup');
    if((exit[0] !== undefined)) { //Split popup subs form
        exit = exit[0];
        if (!exit.classList.contains('exit-popup--visible') && !isClosed('exitClosed')) {
            exit.classList.add('exit-popup--visible');
        }
    }
};

$($('a[target="_blank"][href*="outlink"]')).click(function() {
    let pups = ['first-popup', 'second-popup'];
    pups.forEach(function(elem) {
        let pup = document.getElementsByClassName(elem);
        if(pup[0] !== undefined) {
            pup = pup[0];
            if (!pup.classList.contains(elem + '--visible') && !isClosed(elem)) {
                pup.classList.add(elem + '--visible');
            }
        }
    });
});

let push = processPushPopup();

closeHandle('exitClosed', 'exit-popup',  'visible', true, ['exit-popup__close-btn', 'exit-popup__icon-close'], undefined);
closeHandle('thanksOpinion', 'form-popup-opinion',  'shown', false, ['form-popup__icon-close', 'form-popup__btn'], 'form-popup');
closeHandle('first-popup', 'first-popup',  'visible', true, ['first-popup__icon-close', 'first-popup__close-btn'], undefined);
closeHandle('second-popup', 'second-popup',  'visible', true, ['second-popup__icon-close', 'second-popup__close-btn'], undefined);
closeHandle('comments-form-pup', 'comments-form-pup',  'visible', false, ['comments-form-pup__btn', 'comments-form-pup__close'], undefined);
closeHandle('advertizer-top-pup', 'advertizer-top-pup',  'visible', false, ['advertizer-top-pup__btn', 'advertizer-top-pup__close'], undefined);
closeHandle('push-popup', 'push-popup',  'visible', true, ['push-popup__link'], undefined);

function closeHandle(name, elementClass, visibleClass, withSession, closeElements, closeClass) {
    closeElements.forEach(function(elem) {
        let close_button = $('.' + elementClass).find('.' + elem);
        if (close_button[0] !== undefined) {
            close_button[0].addEventListener('click', function(event) {
                event.preventDefault();
                close(name, elementClass, visibleClass, withSession, closeClass);
            });
        }
    });
}

function isClosed(name) {
    let bannerClosed = sessionStorage.getItem(name);
    if (bannerClosed === null) {
        bannerClosed = false;
    }

    return bannerClosed;
}

function close(name, elementClass, visibleClass, withSession, closeClass) {
    let exit = document.getElementsByClassName(elementClass);
    if(exit[0] !== undefined) {
        if (closeClass !== undefined) {
            elementClass = closeClass;
        }
        exit[0].classList.remove(elementClass + '--' + visibleClass);
        if (withSession) {
            sessionStorage.setItem(name, '1');
        }
    }
}


function processPushPopup() {
    let pup = document.getElementsByClassName('push-popup');

    if(!pup[0]) {
        return false;
    }
    pup = pup[0];

    if (!("Notification" in window)) {
        return false;
    }

    if (typeof Notification !== "undefined" && Notification !== null) {
        let permission = Notification.permission;

        if (permission === "default") {
            pup.classList.add('push-popup--visible');
            return true;
        }
    }

    return false;
}