var g_optin_pid = null
var emailCaptureQTipAPI

class EmailSubscriber {
  constructor (formSelector) {
    let self = this
    this.form = $(formSelector)
    this.emailInputEl = this.form.find('input[type=email]')
    this.g_optin_pid = null
    this._setUpQtip()
  }

  init () {
    let self = this
    this.form.on('submit', (e) => {
      self.validateOptin(e, self)
    })
  }

  validateOptin (evt, self) {
    evt.preventDefault()
    evt.returnValue = false
    const email = $.trim(self.emailInputEl.val())

    if (email.length === 0) {
      self._setOptinError('Email address cannot be empty')
      return false
    }

    if (email.length > 0 && EmailSubscriber._validateEmail(email)) {
      const termsEl = this.form.find('.email_capture_terms')
      if (termsEl.length && !termsEl.is(':checked')) {
        self._setOptinError('Please agree to terms before submitting email address.')
        return false
      }

      const termsBox = $('#form-checkbox');
      const checkBox = $('#form-label');
      if (termsBox.length && !termsBox.is(':checked')) {
        self._setOptinError('Please agree to terms.');
        checkBox.addClass('form__label--error');

        checkBox.click(function () {
          $(this).removeClass('form__label--error');
        });

        return false
      }

      self.emailInputEl.attr('disabled', 'disabled')

      self._setOptinProcessing()
      $.ajax({
        type: 'POST',
        url: '/subscribe/email',
        data: {
          query: location.search,
          email: email,
          site_params: window.__rsconfig.env
        },
        dataType: 'json',
        success: function (response) {
          self.emailInputEl.attr('disabled', '')
          self.emailInputEl.attr('disabled', false)

          if (response.success) {
            if ($('#form-popup-success').length) {
              $('#form-popup-success').addClass('form-popup--shown')
              $('.form-popup__btn,.form-popup__icon-close').on('click', function (e) {
                e.preventDefault()
                $('#form-popup-success').removeClass('form-popup--shown')
              })
            } else {
              self._setOptinSuccess('Thank you for registering!')
            }
          } else {
            let errorText = 'Internal error! Please try again later'
            if (response.errors.email) {
              errorText = response.errors.email[0]
            }
            if ($('#form-popup-failed').length) {

              $('#form-popup-failed .form-popup__text').text(errorText)
              $('#form-popup-failed').addClass('form-popup--shown')
              $('.form-popup__btn,.form-popup__icon-close').on('click', function (e) {
                e.preventDefault()
                $('#form-popup-failed').removeClass('form-popup--shown')

              })
            } else {
              self._setOptinError(errorText);
            }
          }

        }
      })

    } else {
      self._setOptinError('Please enter a valid email address')
    }

    return false
  }

  _setOptinError (aText) {
    this.emailInputEl.removeClass('email_success')
    this.emailInputEl.addClass('email_error')

    if (this.g_optin_pid) {
      clearInterval(this.g_optin_pid)
    }
    this.g_optin_pid = setTimeout(this._hideOptin, 10 * 1000)

    this.emailCaptureQTipAPI.set('content.text', aText)
    this.emailCaptureQTipAPI.toggle(true)
  }

  _setOptinSuccess (aText) {
    this.emailInputEl.removeClass('email_error')
    this.emailInputEl.addClass('email_success')

    if ($('.terms_text').length) {
      $('.terms_text').text(aText)
    } else {
      if (this.g_optin_pid) {
        clearInterval(this.g_optin_pid)
      }
      this.g_optin_pid = setTimeout(this._hideOptin, 10 * 1000)

      this.emailCaptureQTipAPI.set('content.text', aText)
      this.emailCaptureQTipAPI.toggle(true)
    }
  }

  _setOptinProcessing () {
    this.emailInputEl.removeClass('email_error')
    this.emailCaptureQTipAPI.toggle(false)
  }

  _hideOptin () {
    if (this.g_optin_pid) {
      clearInterval(this.g_optin_pid)
    }

    this.emailCaptureQTipAPI.toggle(false)
  }

  _setUpQtip () {
    let self = this
    this.emailCaptureQTipAPI = this.emailInputEl.qtip({
      content: {
        text: ''
      },
      events: {
        show: function (event, api) {
          if (self.g_optin_pid == null) {
            // IE might throw an error calling preventDefault(), so use a try/catch block.
            try {
              event.preventDefault()
            } catch (e) {
            }
          }
        },
        hide: function (event, api) {
          clearInterval(self.g_optin_pid)
          self.g_optin_pid = null
        }
      },
      position: {
        my: 'bottom left',  // Position my top left...
        at: 'top left', // at the bottom right of...
        corner: {
          target: 'topLeft',
          tooltip: 'topLeft'
        },
        target: this.emailInputEl // my target
      }
    }).qtip('api')
  }

  /* email subscribe functions */
  static _validateEmail (email) {
    const re = /^[A-Z0-9._%+-]+@(?:[A-Z0-9-]+\.)+[A-Z]{2,4}$/i
    return re.test(email)
  }

}

$(document).ready(function () {
  const sideBarEmailSubscriber = new EmailSubscriber('#subs-form')
  sideBarEmailSubscriber.init()

//// + legacy - refactor on next interraction
  $('#subscribe-form').on('submit', validateOptin)

  emailCaptureQTipAPI = $('#addEmail_email').qtip({
    content: {
      text: ''
    },
    events: {
      show: function (event, api) {
        if (g_optin_pid == null) {
          // IE might throw an error calling preventDefault(), so use a try/catch block.
          try {
            event.preventDefault()
          } catch (e) {
          }
        }
      },
      hide: function (event, api) {
        clearInterval(g_optin_pid)
        g_optin_pid = null
      }
    },
    position: {
      my: 'bottom center',  // Position my top left...
      at: 'top center', // at the bottom right of...
      target: $('#addEmail_email') // my target
    }
  }).qtip('api')
})

/* email subscribe functions */
function helper_validateEmail (email) {
  var re = /^[A-Z0-9._%+-]+@(?:[A-Z0-9-]+\.)+[A-Z]{2,4}$/i
  return re.test(email)
}

function setOptinError (aText) {
  $('#addEmail_email').removeClass('email_success')
  $('#addEmail_email').addClass('email_error')

  if (g_optin_pid) {
    clearInterval(g_optin_pid)
  }
  g_optin_pid = setTimeout('hideOptin();', 10 * 1000)

  emailCaptureQTipAPI.set('content.text', aText)
  emailCaptureQTipAPI.toggle(true)
}

function setOptinSuccess (aText) {
  $('#addEmail_email').removeClass('email_error')
  $('#addEmail_email').addClass('email_success')

  if ($('.terms_text').length) {
    $('.terms_text').text(aText)
  } else {
    if (g_optin_pid) {
      clearInterval(g_optin_pid)
    }
    g_optin_pid = setTimeout('hideOptin();', 10 * 1000)

    emailCaptureQTipAPI.set('content.text', aText)
    emailCaptureQTipAPI.toggle(true)
  }
}

function setOptinProcessing () {
  $('#addEmail_email').removeClass('email_error')
  emailCaptureQTipAPI.toggle(false)
}

function hideOptin () {
  if (g_optin_pid) {
    clearInterval(g_optin_pid)
  }

  emailCaptureQTipAPI.toggle(false)
}

var validateOptin;
validateOptin = function (evt) {
  evt.preventDefault()
  evt.returnValue = false
  var $form = $(this)
  var email = $.trim($form.find('#addEmail_email').val())

  if (email.length == 0) {
    setOptinError('Email address cannot be empty')
    return false
  }

  if (email.length > 0 && helper_validateEmail(email)) {

    if ($('#email_capture_terms').length && !$('#email_capture_terms').is(':checked')) {
      setOptinError('Please agree to terms before submitting email address.')
      return false
    }

    $('#addEmail_email').attr('disabled', 'disabled')

    setOptinProcessing()
    $.ajax({
      type: 'POST',
      url: '/subscribe/email',
      data: {
        query: location.search,
        email: email,
        site_params: window.__rsconfig.env
      },
      dataType: 'json',
      success: function (response) {
        $('#addEmail_email').attr('disabled', '')
        $('#addEmail_email').attr('disabled', false)

        if (response.success) {
          setOptinSuccess('Thank you for registering!')
        } else if (response.errors.email) {
          setOptinError(response.errors.email[0])
        } else {
          setOptinError('Internal error! Please try again later')
        }

      }
    })

  } else {
    setOptinError('Please enter a valid email address')
  }

  return false
}

/* email subscribe functions - end */