$(function () {
  $('.post__item').slice(0, 4).show()
  $('.load-more__btn').on('click', function (e) {
    e.preventDefault()
    $('li:hidden').slice(0, 4).slideDown()
    if ($('li:hidden').length == 0) {
      $('.load-more').fadeOut('slow')
    }
  })
})

$('.burger').click(function () {
  $('.nav').toggleClass('is-visible');
  $('body').toggleClass('fixed');
  $('.burger__item').toggleClass('is-active');
  $('.burger__inner').toggleClass('burger__inner--pdg');
  if ($(".submenu__list").hasClass("submenu__list--show")) {
    $(".submenu__list").removeClass("submenu__list--show");
  }
});

$('.submenu__link--more').click(function (event) {
  $(event.target).parent(".submenu").find(".submenu__list").addClass('submenu__list--show')
})
$('.submenu__item-back').click(function () {
  $('.submenu__list').removeClass('submenu__list--show')
})

$('.nav__link').click(function () {
  $('.nav__link').removeClass('active')
  $(this).toggleClass('active')
});

//Advertiser disclosure visible

$('.disclosure__btn-wrapp').click(function (e) {
  $(this).next('.advertizer-top-pup').addClass('advertizer-top-pup--visible');
  e.stopPropagation();
  $('body').addClass('fixed');
});
$('.advertizer-top-pup__close').click(function (e) {
  $('.advertizer-top-pup').removeClass('advertizer-top-pup--visible');
  e.stopPropagation();
  $('body').removeClass('fixed');
});
$(document).click(function(e) {
  $('.advertizer-top-pup--visible').removeClass('advertizer-top-pup--visible');
  e.stopPropagation();
  $('body').removeClass('fixed');
});
$('.advertizer-top-pup').click(function (e) {
  $('.advertizer-top-pup--visible').removeClass('advertizer-top-pup--visible');
  e.stopPropagation();
  $('body').removeClass('fixed');
});
$('.advertizer-top-pup__text-block').click(function (e) {
  e.stopPropagation();
});

//Tooltip
$(document).ready(function () {
  $('.table__tooltip').delay(2000).fadeIn(500)
  $('.table__tooltip').delay(10000).fadeOut(500)
});

//Review fixed panel
$(window).scroll(function () {
  var height = $(window).scrollTop()

  if (height > 750) {
    $('.fixed-panel').addClass('fixed-panel--visible')
  } else {
    $('.fixed-panel').removeClass('fixed-panel--visible')
  }

  if (height > 70) {
    $('.fixed-panel').addClass('fixed-panel--m-visible')
  } else {
    $('.fixed-panel').removeClass('fixed-panel--m-visible')
  }
})

//functions to set cookies, get their values and delete them.

function setCookie (c_name, value, exdays, hours) {

  var exdate = new Date()
  exdate.setDate(exdate.getDate() + exdays)
  exdate.setHours(exdate.getHours() + hours)
  var c_value = (escape(JSON.stringify(value)) + ((hours == null) ? '' : '; expires=' + exdate.toUTCString()))
  document.cookie = c_name + '=' + c_value

}

function getCookie (c_name) {
  var c_value = document.cookie
  var c_start = c_value.indexOf(' ' + c_name + '=')

  if (c_start == -1) {
    c_start = c_value.indexOf(c_name + '=')
  }

  if (c_start == -1) {
    c_value = null
  } else {
    c_start = c_value.indexOf('=', c_start) + 1
    var c_end = c_value.indexOf(';', c_start)

    if (c_end == -1) {
      c_end = c_value.length
    }

    c_value = unescape(c_value.substring(c_start, c_end))
  }

  return JSON.parse(c_value)
}

//Facebook share
var fbButton = document.getElementById('fb-share-button')
var url = window.location.href
if (fbButton) {
  fbButton.addEventListener('click', function () {
    window.open('https://www.facebook.com/sharer/sharer.php?u=' + url,
      'facebook-share-dialog',
      'width=800,height=600'
    )
    return false
  })
}

$(document).ready(function(){
  $(".review__btn-block").on("click","a:not(.review__btn)", function (event) {
    event.preventDefault();
    var id  = $(this).attr('href'),
    top = $(id).offset().top - 80;
    $('body,html').animate({scrollTop: top}, 1500);
  });
});

$('.comments-form__textarea').keyup(updateCount);
$('.comments-form__textarea').keydown(updateCount);

function updateCount() {
  var characters = [325- $('.comments-form__textarea').val().length];
  $('.comments-form__characters').text(characters + " symbols left");
}

$(document).ready(function(){

    $('#comments-form').submit(function(event){
      if (!$("#comments-form").valid()) {
       return false;
      }
      event.preventDefault();
      let form = this;
      $.ajax({
        type: "POST",
        url: '/ajax/comment',
        data: $(this).serialize(),
        success: function() {
          $(form).trigger('reset');
          $('.comments-form-pup').addClass('comments-form-pup--visible');
          updateCount();
        }
      });

    });

    $('#top_adv_link').click(function () {
      $('.advertizer-top-pup').addClass('advertizer-top-pup--visible');
    })
});


let onlineList = [];

let cookies = getCookie('_rs_online');
if (cookies) {
  $('.members__online').each(function(key, elem){
    if (cookies[key]) {
      $(elem).show();
    }
  });
} else {
  $('.members__online').each(function () {
    onlineList.push(Math.random() >= 0.5);
  });
  setCookie('_rs_online', onlineList, 0, 3);
  $('.members__online').each(function(key, elem){
    if (onlineList[key]) {
      $(elem).show();
    }
  });
}


$('.table__headline-score').click(function () {
  $('.table__headline-tooltip').toggleClass('table__headline-tooltip--visible');
});
