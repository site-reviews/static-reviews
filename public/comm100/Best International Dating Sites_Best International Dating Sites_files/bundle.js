firebase.initializeApp({
    messagingSenderId: '999719884472'
});
if ('Notification' in window) {
    var messaging = firebase.messaging();
}

function subscribe(successCallback, rejectCallback, onRequestCallback, doneRequestCallback){
    if ('Notification' in window && typeof messaging !== 'undefined') {
        let permission = Notification.permission;
        if (permission === 'default') {
            onRequestCallback();
        }

        messaging.requestPermission()
            .then(function () {
                doneRequestCallback();
                messaging.getToken()
                    .then(function (currentToken) {
                        if (currentToken) {
                            if (!isTokenSentToServer(currentToken)) {
                                setTokenSentToServer(currentToken);
                                successCallback(currentToken);
                            }
                        } else {
                            rejectCallback('cant get token');
                        }
                    })
                    .catch(function (err) {
                        rejectCallback(err);
                    });
            })
            .catch(function (err) {
                doneRequestCallback();
                rejectCallback(err);
            });
    }
}

function setTokenSentToServer(currentToken) {
    window.localStorage.setItem(
        'sentFirebaseMessagingToken',
        currentToken ? currentToken : ''
    );
}

function isTokenSentToServer(currentToken) {
    return window.localStorage.getItem('sentFirebaseMessagingToken') == currentToken;
}

if (typeof messaging !== 'undefined') {
    messaging.onMessage((payload) => {
        console.log('Message received. ', payload);
    });
}

